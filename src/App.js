import React from "react";
import StopWatcher from "./Component";
import { Container } from "react-bootstrap";
function App() {
  return (
    <Container fluid>
      <StopWatcher />
    </Container>
  );
}

export default App;
