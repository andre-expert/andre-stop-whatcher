import React, { useState, useEffect } from "react";
import "./index.css";
import { Container, Row, Col } from "react-bootstrap";

const StopWatcher = () => {
  const [s, setS] = useState(0);
  const [ss, setSs] = useState(0);
  const [sec, setSec] = useState(0);
  const [min, setMin] = useState(0);
  const [hour, setHour] = useState(0);
  const [isActive, setIsActive] = useState(false);

  const handleToggle = () => {
    setIsActive(!isActive);
  };

  const handleRester = () => {
    setS(0);
    setSs(0);
    setSec(0);
    setMin(0);
    setHour(0);
    setIsActive(false);
  };

  useEffect(() => {
    let interval;
    if (isActive) {
      interval = setInterval(() => {
        setS((s) => s + 1);
      }, 10);
    } else if (!isActive && s !== 0) {
      clearInterval(interval);
    }

    return () => {
      clearInterval(interval);
    };
  }, [isActive]);

  if (s === 10) {
    setSs((ss) => ss + 1);
    setS(0);
  }
  if (ss === 10) {
    setSec((sec) => sec + 1);
    setSs(0);
  }
  if (sec === 60) {
    setMin((min) => min + 1);
    setSec(0);
  }
  if (min === 60) {
    setHour((hour) => hour + 1);
    setMin(0);
  }

  return (
    <Container>
      <Row className="justify-content-center mt-5 pt-5">
        <Col lg={6} className="main ">
          <h1 className="text-center p-3 mt-3">My Stop Watcher</h1>
          <Row className="justify-content-center ">
            <Col className="text-center" as="span" lg={8}>
              <h2 className="p-3">
                Hours
                <p> {hour} </p>
              </h2>
              <h2 className="p-3">
                Mins
                <p> {min} </p>
              </h2>
              <h2 className="p-3">
                Seconds
                <p> {sec} </p>
              </h2>
              <h2 className="p-3">
                S.sec
                <p>
                  {ss}.{s}
                </p>
              </h2>
            </Col>
          </Row>
          <Row className="justify-content-center mb-5">
            <button
              onClick={handleToggle}
              className={`${isActive ? "pause" : "start"} m-2 col-3 `}
            >
              <h2>{isActive ? "Pause" : "Start"}</h2>
            </button>

            <button onClick={handleRester} className="reset m-2 col-3">
              <h2>Reset</h2>
            </button>
          </Row>
        </Col>
      </Row>
    </Container>
  );
};

export default StopWatcher;
